package BHX;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Case01 {
    @Test
    public void TestBHX() throws InterruptedException {

        System.out.println("Hi Mr. Nam!\nThis is my work!");

        WebDriver driver;

        WebDriverManager.chromedriver().setup();
        System.out.println("Open Chrome");
        driver = new ChromeDriver();

        System.out.println("Open Bachhoaxanh homepage,");
        driver.get("https://www.bachhoaxanh.com/");
        Thread.sleep(500);
        System.out.println("Search the searching bar,");
        WebElement a = driver.findElement(By.id("text-search"));
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        Thread.sleep(500);
        System.out.println("Click to searching bar,");
        a.click();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        Thread.sleep(500);
        System.out.println("Enter keyword: Gạo,");
        a.sendKeys("Gạo");
        Thread.sleep(1000);
        System.out.println("Submit the keyword,");
        WebElement b = driver.findElement(By.id("text-search"));
        b.submit();
        System.out.println("Searching for the product that has productID is 236057...");
        Thread.sleep(2000);

        try {
        WebElement c = driver.findElement(By.xpath("//li[@data-product='236057']"));
            if (c.getText() != "")
            {
                System.out.println("The product with productID 236057 is show web!");
            }
        } catch (Exception e)
        {
            System.out.println("The product with productID 236057 is NOT show web!");
        }

        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        Thread.sleep(5000);

        driver.quit();
        System.out.println("Close Chrome");
    }
}
